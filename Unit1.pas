unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.Edit, System.ImageList, FMX.ImgList,
  FMX.StdCtrls, FMX.Layouts, FMX.ListBox, System.JSON, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView;

type
  TMainPage = class(TForm)
    Rectangle1: TRectangle;
    Los: TStyleBook;
    SearchBarEdit: TEdit;
    BottomBar: TRectangle;
    BtnPower: TButton;
    BtnSearch: TButton;
    BtnDebug: TButton;
    BtnMenu: TButton;
    BtnLoad: TButton;
    BtnSettings: TButton;
    ListBox1: TListBox;
    OpenDialog_DBLoad: TOpenDialog;
    procedure BtnSearchClick(Sender: TObject);
    procedure BtnPowerClick(Sender: TObject);
    procedure BtnDebugClick(Sender: TObject);
    procedure BtnMenuClick(Sender: TObject);
    procedure BtnLoadClick(Sender: TObject);
    procedure BtnSettingsClick(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
//    procedure OpenDialog_DBLoadSelectionChange(Sender: TObject);
    procedure OpenDialog_DBLoadClose(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainPage: TMainPage;
  sr:TSearchRec;

implementation

{$R *.fmx}
procedure TMainPage.BtnSearchClick(Sender: TObject);
begin
  ListBox1.Items.Clear;
  ///SearchBarEdit.Text
  if (FindFirst('*',faAnyFile,sr)=0) then
  Begin
  repeat
  ListBox1.Items.Add(sr.Name);
  until (FindNext(sr)<>0);
  End;
  FindClose(sr);
  SearchBarEdit.Text:='';
end;

procedure TMainPage.BtnSettingsClick(Sender: TObject);
begin
  ShowMessage('� ����������');
end;

procedure TMainPage.ListBox1DblClick(Sender: TObject);
var LoadDir:string;
begin
 LoadDir:=GetCurrentDir();
 SetCurrentDir(LoadDir + '\' + ListBox1.Items.Strings[ListBox1.ItemIndex]);
 ListBox1.Clear;
 if (FindFirst('*',faAnyFile,sr)=0) then
  Begin
  repeat
  ListBox1.Items.Add(sr.Name);
  until (FindNext(sr)<>0);
  End;
  FindClose(sr);
  SearchBarEdit.Text:='';
end;

procedure TMainPage.OpenDialog_DBLoadClose(Sender: TObject);
begin
      ShowMessage(OpenDialog_DBLoad.FileName + ' was added.');
//    ShowMessage(OpenDialog_DBLoad.FileName);
end;

procedure TMainPage.BtnDebugClick(Sender: TObject);
begin
  ShowMessage('� ����������');
end;

procedure TMainPage.BtnLoadClick(Sender: TObject);
begin
  OpenDialog_DBLoad.Execute;
//  ShowMessage(GetCurrentDir+' ���� ����� ������.');
end;

procedure TMainPage.BtnMenuClick(Sender: TObject);
begin
  ShowMessage('� ����������');
end;

procedure TMainPage.BtnPowerClick(Sender: TObject);
begin
  halt;
end;

end.
